package cn.mcp.auth

data class PlayerUser(
    val id: String,
    val name: String,
    val password: String
)
