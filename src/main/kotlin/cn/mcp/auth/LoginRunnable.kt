package cn.mcp.auth

import org.bukkit.scheduler.BukkitRunnable

class LoginRunnable(private val plugin: AuthPlugin, private val player: String) : BukkitRunnable() {


    override fun run() {
        plugin.onPlayerLoginTimeout(player)
    }
}