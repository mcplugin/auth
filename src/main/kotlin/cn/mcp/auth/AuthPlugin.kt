package cn.mcp.auth

import cn.mcp.auth.cmd.*
import cn.mcp.core.*
import cn.mcp.core.ktx.*
import com.google.gson.reflect.TypeToken
import org.bukkit.Bukkit
import org.bukkit.Location
import org.bukkit.attribute.Attribute
import org.bukkit.entity.Player
import org.bukkit.event.EventHandler
import org.bukkit.event.EventPriority
import org.bukkit.event.Listener
import org.bukkit.event.player.PlayerCommandPreprocessEvent
import org.bukkit.event.player.PlayerJoinEvent
import org.bukkit.event.player.PlayerMoveEvent
import org.bukkit.event.player.PlayerQuitEvent
import org.bukkit.scheduler.BukkitRunnable
import java.io.File

class AuthPlugin : MCPlugin(), Listener {

    private var loginTimeout: Long = 0
    private var onlinespawn: Location? = null
    private var onlinespawnEnable: Boolean = false

    private val database = File(dataFolder, "/database")
    private val databasePlayer = File(database, "players.json")

    // 玩家账号信息
    private val playerUserMap = mutableMapOf<String, PlayerUser>()

    // 需登录玩家
    private val needLogin = mutableListOf<String>()

    override fun onEnable() {
        super.onEnable()
        saveDefaultConfig()
        onlinespawn = config.getSerializable("onlinespawn", Location::class.java)
        onlinespawnEnable = config.getBoolean("onlinespawn-enable", false)
        loginTimeout = config.getLong("login-timeout", 0)

        if (!database.exists()) {
            database.mkdirs()
        }
        if (databasePlayer.exists()) {
            playerUserMap.putAll(
                getGson().fromJson<Map<String, PlayerUser>>(
                    databasePlayer.readText(),
                    object : TypeToken<Map<String, PlayerUser>>() {}.type
                )
            )
        }
        addCommandExecutors(
            RegisterCommand(this),
            LoginCommand(this),
            PasswdCommand(this),
            ResetCommand(this),
            SetOnlinespawnCommand(this)
        )
        registerEvents(this)

    }

    @EventHandler(priority = EventPriority.MONITOR)
    fun onPlayerCommandEvent(event: PlayerCommandPreprocessEvent) {
        if (needLogin.contains(event.player.name)) {
            val msg = event.message
            if (!msg.startsWith("/login") && !msg.startsWith("/register")) {
                event.isCancelled = true
            }
        }
    }

    /**
     * 玩家加入游戏
     * 1.隐藏此玩家
     * 2.禁止玩家行动
     * 3.判断玩家注册过
     * 4.根据注册情况提示使用对应的命令
     */
    @EventHandler
    fun onPlayerJoin(event: PlayerJoinEvent) {
        event.joinMessage = ""
        val player = event.player

        if (loginTimeout > 0) {
            LoginRunnable(this, player.name).runTaskLaterAsynchronously(this, loginTimeout)
        }

        if (onlinespawnEnable) {
            onlinespawn?.run {
                player.teleport(this)
            }
        }

        player.getAttribute(Attribute.GENERIC_ATTACK_SPEED)

        // 设置玩家不可移动
        needLogin.add(player.name)
        // 隐藏此玩家
        player.invisibility(this)

        // 判断玩家已经注册
        val message = if (isRegister(player.name)) {
            "使用命令 /login 登录"
        } else {
            "使用命令 /register 注册"
        }
        player.sendMessage(buildSystemChat(message))
    }

    /**
     * 玩家离开游戏
     */
    @EventHandler
    fun onPlayerQuit(event: PlayerQuitEvent) {
        // 解除玩家不可移动状态
        needLogin.remove(event.player.name)

        event.quitMessage = buildBroadcastChat("${event.player.name} 离开了游戏")
    }

    /**
     * 玩家移动
     * 此处用于限制玩家未登录前不可移动
     */
    @EventHandler(priority = EventPriority.MONITOR)
    fun onPlayerMove(event: PlayerMoveEvent) {
        if (needLogin.isNotEmpty() && needLogin.contains(event.player.name)) {
            event.isCancelled = true
        }
    }

    /**
     * 判断玩家是否曾经注册过
     */
    private fun isRegister(playerName: String): Boolean = playerUserMap.containsKey(playerName)

    /**
     * 玩家注册到服务器
     */
    fun onRegister(player: Player, password1: String, password2: String) {
        if (isRegister(player.name)) {
            player.sendActionbarMessage("此账号已被注册")
            return
        }

        if (password1 != password2) {
            player.sendActionbarMessage("密码不一致，请重新注册")
            return
        }

        val name = player.name
        playerUserMap[name] = PlayerUser(player.uniqueId.toString(), name, password1.md5())
        databasePlayer.writeText(getGson().toJson(playerUserMap))
        player.sendActionbarMessage("注册成功，使用命令 /login 登录")
    }

    /**
     * 玩家登录到服务器
     */
    fun onLogin(player: Player, password: String) {
        if (!isRegister(player.name)) {
            player.sendActionbarMessage("此账号还未注册，请先注册")
            return
        }
        val playerUser = playerUserMap[player.name]
        if (password.md5() != playerUser?.password) {
            player.sendActionbarMessage("密码不正确")
            return
        }

        if (!needLogin.contains(player.name)) {
            player.sendActionbarMessage("当前已登录，请勿重复登录")
            return
        }

        onLoginSuccess(player)
    }

    /**
     * 玩家登录成功
     */
    private fun onLoginSuccess(player: Player) {
        // 解除玩家不可移动状态
        needLogin.remove(player.name)

        player.show(this)

        player.sendActionbarMessage("登录成功")

        // 广播提醒玩家上线
        Bukkit.broadcastMessage(buildBroadcastChat("${player.name} 加入游戏"))
    }

    /**
     * 修改密码
     */
    fun onPasswd(player: Player, oldPassword: String, password1: String, password2: String) {
        if (!isRegister(player.name)) {
            player.sendActionbarMessage("此账号还未注册，请先注册")
            return
        }

        val playerUser = playerUserMap[player.name]
        if (oldPassword.md5() != playerUser?.password) {
            player.sendActionbarMessage("原密码不正确")
            return
        }

        if (password1 != password2) {
            player.sendActionbarMessage("新密码不一致")
            return
        }

        val name = player.name
        playerUserMap[name] = PlayerUser(player.uniqueId.toString(), name, password1.md5())
        databasePlayer.writeText(getGson().toJson(playerUserMap))
        player.sendActionbarMessage("密码修改成功")
    }

    /**
     * 重置密码
     */
    fun onReset(op: Player, playerName: String, password: String) {
        if (!isRegister(playerName)) {
            op.sendActionbarMessage("此账号还未注册，请先注册")
            return
        }

        val player = Bukkit.getPlayer(playerName)
        if (player == null) {
            op.sendActionbarMessage("玩家不存在")
            return
        }

        playerUserMap[playerName] = PlayerUser(player.uniqueId.toString(), playerName, password.md5())
        databasePlayer.writeText(getGson().toJson(playerUserMap))
        op.sendActionbarMessage("玩家 $playerName 密码重置成功")
    }

    /**
     * 设置上线坐标
     */
    fun onSetOnlinepoint(player: Player) {
        val location = player.location
        config.set("onlinespawn", location)
        config.options()
        saveConfig()

        player.sendActionbarMessage("设置上线点成功")

        onlinespawn = location
    }

    /**
     * 当玩家登录超时
     */
    fun onPlayerLoginTimeout(player: String) {
        if (needLogin.contains(player)) {
            Bukkit.getPlayer(player)?.run {
                if (isOnline) {
                    object : BukkitRunnable() {
                        override fun run() {
                            kickPlayer("登录超时")
                        }
                    }.runTask(this@AuthPlugin)
                }
            }
        }
    }
}