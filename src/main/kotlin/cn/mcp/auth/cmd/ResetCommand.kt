package cn.mcp.auth.cmd

import cn.mcp.auth.AuthPlugin
import cn.mcp.core.cmd.PlayerCommandExecutor
import org.bukkit.command.Command
import org.bukkit.entity.Player

class ResetCommand(private val plugin: AuthPlugin) : PlayerCommandExecutor() {

    override fun getCommandName(): String = "reset"

    override fun onCommand(player: Player, command: Command, label: String, args: Array<out String>): Boolean {
        if (args.size != 2) {
            return false
        }

        plugin.onReset(player, args[0], args[1])

        return true
    }

}