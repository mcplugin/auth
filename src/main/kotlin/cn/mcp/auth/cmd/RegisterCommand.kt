package cn.mcp.auth.cmd

import cn.mcp.auth.AuthPlugin
import cn.mcp.core.cmd.PlayerCommandExecutor
import org.bukkit.command.Command
import org.bukkit.entity.Player

class RegisterCommand(private val plugin: AuthPlugin) : PlayerCommandExecutor() {

    override fun onCommand(player: Player, command: Command, label: String, args: Array<out String>): Boolean {
        if (args.size < 2 || args.size > 2) {
            return false
        }
        plugin.onRegister(player, args[0], args[1])
        return true
    }

    override fun getCommandName(): String = "register"
}