package cn.mcp.auth.cmd

import cn.mcp.auth.AuthPlugin
import cn.mcp.core.cmd.PlayerCommandExecutor
import org.bukkit.command.Command
import org.bukkit.entity.Player

class PasswdCommand(private val plugin: AuthPlugin) : PlayerCommandExecutor() {

    override fun onCommand(player: Player, command: Command, label: String, args: Array<out String>): Boolean {
        if (args.size != 3) {
            return false
        }
        plugin.onPasswd(player, args[0], args[1], args[2])
        return true
    }

    override fun getCommandName(): String = "passwd"
}