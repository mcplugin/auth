package cn.mcp.auth.cmd

import cn.mcp.auth.AuthPlugin
import cn.mcp.core.cmd.PlayerCommandExecutor
import org.bukkit.command.Command
import org.bukkit.entity.Player

class LoginCommand(private val plugin: AuthPlugin) : PlayerCommandExecutor() {

    override fun onCommand(player: Player, command: Command, label: String, args: Array<out String>): Boolean {
        if (args.size != 1) {
            return false
        }
        val password = args[0]
        plugin.onLogin(player, password)
        return true
    }

    override fun getCommandName(): String = "login"
}