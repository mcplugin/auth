package cn.mcp.auth.cmd

import cn.mcp.auth.AuthPlugin
import cn.mcp.core.cmd.PlayerCommandExecutor
import org.bukkit.command.Command
import org.bukkit.entity.Player

class SetOnlinespawnCommand(private val plugin: AuthPlugin) : PlayerCommandExecutor() {
    override fun onCommand(player: Player, command: Command, label: String, args: Array<out String>): Boolean {
        if (args.isNotEmpty()) {
            return false
        }
        plugin.onSetOnlinepoint(player)
        return true
    }

    override fun getCommandName(): String = "setonlinespawn"
}